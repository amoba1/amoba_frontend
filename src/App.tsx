import {useContext, useEffect, useState} from 'react';
import GameBoard from "./components/GameBoard";
import Player from "./components/Player";
import axios from "axios";
import {SessionContext} from "./components/sessionId/SessionContext.tsx";
import GameOver from "./components/GameOver.tsx";


function App() {
    const [activePlayer, setActivePlayer] = useState<boolean>(false);
    const {sessionId, gameTurn,winner} = useContext(SessionContext);

    async function handleSelectSquare(rowIndex: number, columnIndex: number){
        try {
            await axios.post(`/api/game/${sessionId}/play`, {
                x: rowIndex,
                y: columnIndex,
            });
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
            setActivePlayer(prevState => !prevState);
    },[gameTurn]);

    function handleRestart () {
        window.location.reload();
    }

    return (
        <main>
            <div id="game-container">
                <ol id="players" className="highlight-player">
                    <Player playerName='Player 1' playerSymbol='X'  isActive={activePlayer}></Player>
                    <Player playerName='Player 2' playerSymbol='O' isActive={!activePlayer}></Player>
                </ol>
                {winner !== '' &&
                    <GameOver winner={`${winner === "draw" ? "DRAW" : (activePlayer ? 'Player 2 (O) WON' : 'Player 1 (X) WON')}`} onRestart={handleRestart}/>
                }
                    <GameBoard onSelectSquare={handleSelectSquare}/>
            </div>
        </main>
    );
}

export default App;
