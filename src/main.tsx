import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import HomePage from "./components/HomePage.tsx";
import SessionContextProvider from "./components/sessionId/SessionContextProvider.tsx";

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <SessionContextProvider>
        <HomePage/>
      </SessionContextProvider>
  </React.StrictMode>,
)
