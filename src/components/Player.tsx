interface PlayerProps {
    playerName: string,
    playerSymbol: string,
    isActive: boolean,
}
export default function Player(props: PlayerProps){

    return (
        <li className={props.isActive ? 'active' : undefined}>
            <span className='player'>
                {props.playerName}
                <span className='player-symbol'>{props.playerSymbol}</span>
            </span>
        </li>
    )

}