interface GameOverProps {
    winner?: string
    onRestart: () => (void)
}

export default function GameOver(props: GameOverProps) {
    return (
        <div id="game-over">
            <h2>Game Over!</h2>
            {props.winner && <p>{props.winner}</p>}
            <p><button onClick={props.onRestart}>New game!</button></p>
        </div>
    )
}