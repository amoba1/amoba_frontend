import {useContext, useEffect, useState} from "react";
import axios from "axios";
import {SessionContext} from "./sessionId/SessionContext.tsx";

interface GameBoardType {
    onSelectSquare: (rowIndex: number, columnIndex: number) => (void),
}

function GameBoard({onSelectSquare}: GameBoardType) {
    const [gameBoard, setGameBoard] = useState<string[][]>([]);
    const [gameEnded, setGameEnded] = useState<boolean>(false);
    const [clickedOnYourTurn, setClickedOnYourTurn] = useState(false);
    const {sessionId, setGameTurn, gameTurn, setWinner} = useContext(SessionContext);

    useEffect(() => {
        if (gameEnded) {
            return ;
        }

        const intervalId = setInterval(() => fetchGameBoard(), 1000);

        return () => {
            clearInterval(intervalId);
        }
    }, [gameEnded]);

    async function fetchGameBoard(){
        const result = await axios.get(`/api/game/${sessionId}`);
        setGameBoard(result.data.state);
        setGameTurn(result.data.turn);
        setClickedOnYourTurn(false);
        if (result.data.winner === sessionId) {
            setWinner(sessionId);
            setGameEnded(true);
        } else if (result.data.winner && result.data.winner !== sessionId && result.data.winner !== "draw") {
            setWinner(sessionId);
            setGameEnded(true);
        } else if (result.data.winner === "draw") {
            setWinner("draw");
            setGameEnded(true);
        }
    }

    function handleOnClick (rowIndex: number, columnIndex: number) {
        onSelectSquare(rowIndex, columnIndex)
        setClickedOnYourTurn(true)
    }


    return (
        <ol id="game-board">
            {gameBoard && gameBoard.map((row, rowIndex) => <li key={rowIndex}>
                <ol>
                    {row.map((playerSymbol, columnIndex) => <li key={columnIndex}>
                        <button onClick={() => handleOnClick(rowIndex,columnIndex)} disabled={gameBoard[rowIndex][columnIndex] !== "" || gameTurn !== sessionId || clickedOnYourTurn}>{playerSymbol}</button>
                    </li>)}
                </ol>
            </li>)}
        </ol>
    );

}

export default GameBoard;
