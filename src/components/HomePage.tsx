import App from "../App.tsx";
import {useContext, useEffect, useState} from "react";
import axios from "axios";
import {SessionContext} from "./sessionId/SessionContext.tsx";

export default function HomePage(){
    const [findMatch, setFindMatch] = useState<boolean>(false);
    const [waiting, setWaiting] = useState<boolean>(false);
    const {sessionId,changeSessionId} = useContext(SessionContext);

    async function getGame() {
        const result = await axios.post('/api/lobby/');
        changeSessionId(result.data.sessionId);
        if (!result.data.isFirstPlayerInTheLobby) {
            setWaiting(true);
        } else {
            setFindMatch(true);
        }
    }

    useEffect(() => {
        if (!waiting) {
            return;
        } else if (waiting && sessionId) {
            const intervalId = setInterval(() => fetchOpponent(), 1000);
            return () => clearInterval(intervalId);
        }
    }, [waiting]);

    async function fetchOpponent () {
        try {
            const result = await axios.get(`/api/game/${sessionId}`);
            if (result.data.state) {
                setFindMatch(true);
                setWaiting(false);
            }
        } catch (e) {
            console.log(e);
        }
    }

    async function handleFindMatch () {
        await getGame();
    }

    return (
        <>
            {findMatch ?
                <App/> :
                    <div id="game-over">
                    {waiting ?
                    <p style={{color: "white"}}>Waiting for other players to connect...</p>    :
                    <button onClick={handleFindMatch}>Find match</button>
                    }
                    </div>
            }
        </>
    );
}